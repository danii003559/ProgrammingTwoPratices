package task4;

public class Solution {

    public static int sumGroups(int[] arr) {
        int sum = 0, index = 0;
        boolean estatus = typeNumber(arr[0]);

        for (int i = 0; i < arr.length; i++) {
            if (typeNumber(arr[i]) == estatus) {
                sum += arr[i];
            } else {
                estatus = typeNumber(arr[i]);

                if (i != arr.length - 1 && typeNumber(arr[i]) == typeNumber(arr[i + 1])) {
                    sum += arr[i];
                }
            }
            if (i != arr.length - 1 && (typeNumber(arr[i]) == typeNumber(arr[i + 1])) == false) {
                index++;
                if (sum == 0) {
                    arr[index] = arr[i];
                } else {
                    arr[index] = sum;
                }
                sum = 0;
            }

        }
        if (arr.length < 3) {
            index++;
        }
        return index;
    }

    public static boolean typeNumber(int number) {
        return number % 2 == 0;
    }
}