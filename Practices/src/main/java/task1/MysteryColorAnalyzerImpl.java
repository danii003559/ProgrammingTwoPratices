package task1;

import java.util.HashSet;
import java.util.List;

public class MysteryColorAnalyzerImpl  implements MysteryColorAnalyzer{
    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        HashSet<Color> uniqueColors = new HashSet<>(mysteryColors);
        return uniqueColors.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int repetition = 0;
        for(Color listColor : mysteryColors){
            if(listColor.equals(color)){
                repetition++;
            }
        }
        return repetition;
    }
}
