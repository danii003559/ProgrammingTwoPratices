package task2;

import java.util.ArrayList;


public class Canvas {
    private int width;
    private int height;
    private ArrayList<String> panel;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        panel = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            panel.add(" ".repeat(width));
        }
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        int number = x1;
        String[] fila;
        if (y1 == y2 || x1 == x2) {
            if (y1 == y2) {
                fila = panel.get(y1).split("");
                number = x1;
                while (x2 >= number) {
                    fila[number] = "x";
                    number++;
                }
                panel.set(y1, String.join("", fila));
            } else if (x1 == x1) {
                number = y1;
                while (number <= y2) {
                    fila = panel.get(number).split("");
                    fila[x1] = "x";
                    panel.set(number, String.join("", fila));
                    number++;
                }
            }
        } else {
            for (int i = y1; i <= y2; i++) {
                fila = panel.get(i).split("");
                if (i == y1 || i == y2) {
                    number = x1;
                    while (x2 >= number) {
                        fila[number] = "x";
                        number++;
                    }
                    panel.set(i, String.join("", fila));
                } else {
                    fila[x1] = "x";
                    fila[x2] = "x";
                    panel.set(i, String.join("", fila));
                }
            }
        }

        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        String fila = panel.get(y);
        String countOfX = fila.replace(" ", "");
        if (countOfX.length() == 2) {
            int firstX = fila.indexOf("x");
            int secondX = fila.lastIndexOf("x") + 1;
            if (x > firstX && x < secondX) {
                fila = fila.substring(firstX + 1, secondX - 1);
                String row = "";
                int i = 0;
                while (i < height) {
                    row = panel.get(i);
                    if (!row.replace(" ", "").equals("")) {
                        panel.set(i, row.replace(fila, String.valueOf(ch).repeat(fila.length())));
                    }
                    i++;
                }
            }
        }
        return this;
    }


    public String drawCanvas() {
        String borderHeight = "-".repeat(width + 2);
        String canvasMade = borderHeight + "\n";
        for (int i = 0; i < panel.size(); i++) {
            canvasMade += "|" + panel.get(i) + "|" + "\n";
        }
        canvasMade += borderHeight;
        return canvasMade;
    }
}