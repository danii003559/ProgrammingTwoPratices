package task3;

import java.util.HashMap;
import java.util.Map;

public class Kata {
    public static int mostFrequentItemCount(int[] collection) {
        Map<Integer, Integer> repetitions = new HashMap<>();
        int aux, biggerNumber = 0;
        for (int i = 0; i < collection.length; i++) {
            if (repetitions.containsKey(collection[i])) {
                aux = repetitions.get(collection[i]) + 1;
                repetitions.put(collection[i], aux);
                if (aux > biggerNumber) biggerNumber = aux;
            } else {
                repetitions.put(collection[i], 1);
                if (biggerNumber < 1) biggerNumber = 1;
            }
        }
        return biggerNumber;
    }
}