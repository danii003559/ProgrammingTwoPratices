package task1;



import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class MysteryColorTest {
    MysteryColorAnalyzerImpl mysteryColorAnalyzerImpl  = new MysteryColorAnalyzerImpl();
    List<Color> colors = new ArrayList<>();

    @Test
    public void numberOfDistinctColorsTest(){
        colors.add(Color.BLUE);
        colors.add(Color.GREEN);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.RED);

        int numberOfColor = mysteryColorAnalyzerImpl.numberOfDistinctColors(colors);
        Assert.assertEquals(3,numberOfColor);
    }

    @Test
    public void colorOccurrenceTest(){
        numberOfDistinctColorsTest();
        int numberRepetition = mysteryColorAnalyzerImpl.colorOccurrence(colors,Color.GREEN);
        Assert.assertEquals(2,numberRepetition);

        numberRepetition = mysteryColorAnalyzerImpl.colorOccurrence(colors,Color.BLUE);
        Assert.assertEquals(1,numberRepetition);

        numberRepetition = mysteryColorAnalyzerImpl.colorOccurrence(colors,Color.RED);
        Assert.assertEquals(2,numberRepetition);
    }

}
