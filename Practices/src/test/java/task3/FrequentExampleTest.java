package task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrequentExampleTest {
    @Test
    public void tests() {
        assertEquals(2, Kata.mostFrequentItemCount(new int[] {3, -1, -1}));
        assertEquals(5, Kata.mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
        assertEquals(0, Kata.mostFrequentItemCount(new int[] {}));
        assertEquals(1, Kata.mostFrequentItemCount(new int[] {5}));
    }
}